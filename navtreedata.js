/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "My Project", "index.html", [
    [ "Introduction", "index.html#section_intro", null ],
    [ "Lab0x01", "page_lab1.html", [
      [ "Fibonacci.py Source Code Access", "page_lab1.html#sec_fibsource", null ],
      [ "Fibbonacci.py Documentation", "page_lab1.html#sec_fibdoc", null ]
    ] ],
    [ "Lab0x02", "page_lab2.html", [
      [ "Lab0x02.py Source Code Access", "page_lab2.html#sec_sourcecode2", null ],
      [ "Lab0x02.py Documentation", "page_lab2.html#sec_doc2", null ],
      [ "Instructions", "page_lab2.html#sec_instructions_lab2", null ]
    ] ],
    [ "Lab0x03", "page_lab3.html", [
      [ "Lab0x03.py Source Code Access", "page_lab3.html#sec_sourcecode3", null ],
      [ "Lab0x03.py Documentation", "page_lab3.html#sec_doc3", null ]
    ] ],
    [ "Term Project", "page_termproject.html", [
      [ "Week 1: main.py", "page_termproject.html#sec_main", null ],
      [ "Week 1: UI_dataGen.py", "page_termproject.html#sec_UI_dataGen", null ],
      [ "Week 1: Lab0xFF.py", "page_termproject.html#sec_Lab0xFF", null ],
      [ "Week 2: encoder.py", "page_termproject.html#sec_encoder", null ],
      [ "Week 2: encoderTask.py", "page_termproject.html#sec_encoderTask", null ],
      [ "Week 3: MotorDriver.py", "page_termproject.html#sec_MotorDriver", null ],
      [ "Task Diagram for all Term Project Files", "page_termproject.html#sec_term_diagram", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Elevator_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';